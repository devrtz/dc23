---
title: Siemens Platinum Sponsor of DebConf23
---

We are pleased to announce that [**Siemens**](https://www.siemens.com) has
committed to sponsor [DebConf23](https://debconf23.debconf.org/) as
**Platinum Sponsor**.

Siemens is a technology company focused on industry, infrastructure and
transport. From more resource-efficient factories, resilient supply
chains, and smarter buildings and grids, to cleaner and more comfortable
transportation as well as advanced healthcare, the company creates
technology with purpose adding real value for customers. By combining
the real and the digital worlds, Siemens empowers its customers to
transform their industries and markets, helping them to transform the
everyday for billions of people.

With this commitment as Platinum Sponsor, Siemens is contributing to
make possible our annual conference, and directly supporting the
progress of Debian and Free Software, helping to strengthen the
community that continues to collaborate on Debian projects throughout
the rest of the year.

Thank you very much Siemens, for your support of DebConf23!
