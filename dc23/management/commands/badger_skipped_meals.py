from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import Template, Context

from register.models import Attendee, Food, Meal
from register.dates import skipped_meals


SUBJECT = '[DebConf 23]: Some meals will not be served'
TXT = '''
Dear {{name}},

We see from our records that you were registered for some meals that the
conference will no longer be providing:
{% for meal in meals %}{{ meal.date }}: {{ meal.meal|title }}
{% endfor %}
We have removed them from your registration.  Please make alternative plans.
Instead of eating at the venue at those times, we will probably walk into town
and find something to eat. You are welcome to join us for that.

{% if paid %}
If you have paid for these meals yourself, rather than having them covered by a
food bursary, please contact the registration team, and we can organise a
refund.{% endif %}

Thanks,

The DebConf Registration Team
'''


class Command(BaseCommand):
    help = "Badger attendees who booked meals that we're cancelling"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, registered_skipped_meals, dry_run):
        to = attendee.user.email
        name = attendee.user.get_full_name()

        if dry_run:
            print(f'I would badger {name} <{to}>')
            return

        ctx = Context({
            'name': name,
            'paid': attendee.billable() and attendee.paid(),
            'meals': registered_skipped_meals,
        })

        subject = Template(SUBJECT).render(ctx).strip()
        body = Template(TXT).render(ctx)

        email_message = EmailMultiAlternatives(subject, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()
        for meal in registered_skipped_meals:
            attendee.food.meals.remove(meal)

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        skipped_meal_ids = []
        skipped_meal_set = set(skipped_meals())
        for meal in Meal.objects.all():
            if meal.form_name in skipped_meal_set:
                skipped_meal_ids.append(meal.id)

        for attendee in Attendee.objects.all():
            if not attendee.user.userprofile.is_registered():
                continue
            try:
                registered_skipped_meals = attendee.food.meals.filter(
                    id__in=skipped_meal_ids)
            except Food.DoesNotExist:
                continue
            if not registered_skipped_meals.exists():
                continue
            self.badger(attendee, registered_skipped_meals, dry_run)
