from datetime import date, datetime, time

from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import Template, Context
from django.utils.timezone import make_aware

from register.models import Attendee, Food, Meal


SUBJECT = '[DebConf 23]: Conference Dinner date has changed'
TXT = '''
Dear {{name}},

Our apologies; the registration system had the wrong date for the conference
dinner. It will now be on {{ NEW_CONFERENCE_DINNER_DAY|date:"Y-m-d" }}.

From our registration records, we see that:
{% if only_dinner %}You were only registered for the conference dinner on {{ OLD_CONFERENCE_DINNER_DAY|date:"Y-m-d" }},
not other dinners.
{% elif skipped_dinner %}You were intentionally skipping the conference dinner on {{ OLD_CONFERENCE_DINNER_DAY|date:"Y-m-d" }}.
{% endif %}
{% if moved %}We have updated your registration to register you for the conference dinner on
{{ NEW_CONFERENCE_DINNER_DAY|date:"Y-m-d" }} instead of {{ OLD_CONFERENCE_DINNER_DAY|date:"Y-m-d" }}.
{% elif moved_gap %}We have updated your registration to register you for (normal) dinner on
{{ OLD_CONFERENCE_DINNER_DAY|date:"Y-m-d" }} and cancelled your dinner registration on
{{ NEW_CONFERENCE_DINNER_DAY|date:"Y-m-d" }}, avoiding the new conference dinner.
{% elif cancelled %}We have cancelled your registration for dinner on {{ OLD_CONFERENCE_DINNER_DAY|date:"Y-m-d" }}.
{% else %}We haven't made any changes to your meal selections, as we can't determine the correct change to make.
{% endif %}

{% if moved or moved_gap or cancelled %}We hope we made the correct changes for you.
{% endif %}
Please check your meal selections in registration, and complete the form again:
<https://{{ WAFER_CONFERENCE_DOMAIN }}/register/>

{% if billable %}This may have affected your costs.{% if paid %}
If you need a refund, please contact the registration team by email.{% endif %}{% endif %}

Thanks,

The DebConf Registration Team
'''

NEW_CONFERENCE_DINNER_DAY = date(2023, 9, 14)
OLD_CONFERENCE_DINNER_DAY = date(2023, 9, 15)
DINNER_START_TIME = time(19, 0)
DINNER_END_TIME = time(22, 30)
NEW_DINNER_START = make_aware(
    datetime.combine(NEW_CONFERENCE_DINNER_DAY, DINNER_START_TIME))
NEW_DINNER_END = make_aware(
    datetime.combine(NEW_CONFERENCE_DINNER_DAY, DINNER_END_TIME))
OLD_DINNER_START = make_aware(
    datetime.combine(OLD_CONFERENCE_DINNER_DAY, DINNER_START_TIME))
OLD_DINNER_END = make_aware(
    datetime.combine(OLD_CONFERENCE_DINNER_DAY, DINNER_END_TIME))

class Command(BaseCommand):
    help = "Move the conference dinner to a new date, after opening registration"

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),

    def badger(self, attendee, registered_old_dinner, registered_new_dinner,
               dry_run, site):
        to = attendee.user.email
        name = attendee.user.get_full_name()

        # The only selected dinner is the conference dinner
        only_dinner = (
            attendee.food.meals.filter(meal='dinner').count() == 1
            and registered_old_dinner)

        # The attendee isn't present for the new dinner date
        new_outside_attendance = False
        if attendee.arrival and attendee.arrival > NEW_DINNER_END:
            new_outside_attendance = True
        if attendee.departure and attendee.departure < NEW_DINNER_START:
            new_outside_attendance = True

        # The attendee isn't present for the old dinner date
        old_outside_attendance = False
        if attendee.arrival and attendee.arrival > OLD_DINNER_END:
            old_outside_attendance = True
        if attendee.departure and attendee.departure < OLD_DINNER_START:
            old_outside_attendance = True

        unreliable_attendance = new_outside_attendance and old_outside_attendance

        # The attendee intentionally skipped the conference dinner before
        skipped_dinner = False
        if not registered_old_dinner and not new_outside_attendance:
            other_dinners = attendee.food.meals.filter(meal='dinner').exists()
            if other_dinners:
                skipped_dinner = True

        # We will change the attendee's registered dinners to move the dinner
        moved = only_dinner and (not new_outside_attendance or unreliable_attendance)

        # We will change the attendee's registered idnners to avoid the dinner
        moved_gap = skipped_dinner and (not old_outside_attendance or unreliable_attendance)

        # We will cancel the attendee's registered conference dinner
        cancelled = only_dinner and new_outside_attendance and not unreliable_attendance

        stats = {
            'only_dinner': only_dinner,
            'skipped_dinner': skipped_dinner,
            'new_outside_attendance': new_outside_attendance,
            'old_outside_attendance': old_outside_attendance,
            'unreliable_attendance': unreliable_attendance,
            'moved': moved,
            'moved_gap': moved_gap,
            'cancelled': cancelled,
        }
        if dry_run:
            stats_words = ' '.join([k for k, v in stats.items() if v])
            print(f'I would badger {name} <{to}>', stats_words)
            return stats

        ctx = Context({
            'WAFER_CONFERENCE_DOMAIN': site.domain,
            'NEW_CONFERENCE_DINNER_DAY': NEW_CONFERENCE_DINNER_DAY,
            'OLD_CONFERENCE_DINNER_DAY': OLD_CONFERENCE_DINNER_DAY,
            'name': name,
            'only_dinner': only_dinner,
            'skipped_dinner': skipped_dinner,
            'moved': moved,
            'moved_gap': moved_gap,
            'cancelled': cancelled,
            'billable': attendee.billable(),
            'paid': attendee.paid(),
        })

        subject = Template(SUBJECT).render(ctx).strip()
        body = Template(TXT).render(ctx)

        email_message = EmailMultiAlternatives(subject, body,
                                               to=["%s <%s>" % (name, to)])
        email_message.send()
        new_meal = Meal.objects.get(date=NEW_CONFERENCE_DINNER_DAY,
                                    meal='dinner')
        old_meal = Meal.objects.get(date=OLD_CONFERENCE_DINNER_DAY,
                                    meal='dinner')
        if moved or cancelled:
            attendee.food.meals.remove(old_meal)
        if moved:
            attendee.food.meals.add(new_meal)
        if moved_gap:
            attendee.food.meals.add(old_meal)
            attendee.food.meals.remove(new_meal)

        return stats

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        site = Site.objects.get(id=1)

        old_meal_id = None
        new_meal_id = None
        for meal in Meal.objects.all():
            if meal.meal == 'dinner':
                if meal.date == OLD_CONFERENCE_DINNER_DAY:
                    old_meal_id = meal.id
                elif meal.date == NEW_CONFERENCE_DINNER_DAY:
                    new_meal_id = meal.id

        stats = {
            'unaffected': 0,
            'only_dinner': 0,
            'skipped_dinner': 0,
            'new_outside_attendance': 0,
            'old_outside_attendance': 0,
            'unreliable_attendance': 0,
            'moved': 0,
            'moved_gap': 0,
            'cancelled': 0,
        }

        for attendee in Attendee.objects.all():
            try:
                registered_old_dinner = attendee.food.meals.filter(
                        id=old_meal_id).exists()
                registered_new_dinner = attendee.food.meals.filter(
                        id=new_meal_id).exists()
            except Food.DoesNotExist:
                stats['unaffected'] += 1
                continue
            if not registered_old_dinner and not registered_new_dinner:
                stats['unaffected'] += 1
                continue
            if registered_old_dinner and registered_new_dinner:
                stats['unaffected'] += 1
                continue
            user_stats = self.badger(
                attendee, registered_old_dinner, registered_new_dinner,
                dry_run, site)
            for key, value in user_stats.items():
                if value:
                    stats[key] += 1
        print(stats)
