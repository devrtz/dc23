---
name: Mobile-friendly Schedule
---
# Mobile-friendly Schedule

The schedule should be viewable on a mobile device.
But there are also alternative exports you can use to consume it from
another application.

## XML:

The Schedule is available through an XML feed.
You can use _ConfClerk_ in Debian to consume this, or _Giggity_ on
Android.

<img class="img-fluid" src="{% static "img/dc23-schedule-qr-code.png" %}"
     title="DebConf23 Schedule">

[Add DebConf23 to Giggity](https://ggt.gaa.st#url=https://debconf23.debconf.org/schedule/pentabarf.xml&json=H4sIAAAAAAACA7VUy27bMBC89ysEnlpAEvVoYks3I0GAIijaXpJD4MOKpG3CfFWk3Ierfy9pOwXdtKh16InEkDM7O1pqj3ast1wr1FZFVRdNURdlioZeoBZtnDO2xZiyjmi1qur8tMt1v8aWbBgdBMOGKQcd9Kv8qxQoRY47wTz9lnU3_nJS1R60DnrnwVAlK5qsLDzIFI2h2YEs2XetAn9hOeB7LbbgwJ9I5oCGbbtHnATH_zLowHGCuVzjFewC5XnNymqe9esOcqPWXlpwtbWofdr_8v7IOssdQ5dFgcY0ovItf8H74sFA4aAOjFM22GcTcx-YGphN3oMJUXwzAQNjBCe-Fe_f0NWFlqLmKfH57oJyJsHkQSOuuZBMccf_T1l4Fj8rfYkSdHpwmGiCo5G60ZQlepX47OhA3ESxgIA0-LcZ9dBEISBES6npMZ1IbhEfTG11wwUl0LOzhgOYBHRycDtOy-ZM68PDu9usbP6g9PfxxHfwOdK4W3yaaOQweLGNw4xPFeEW7JlIANC4TFGvtTy-XeWHLXwDvw40PEEBToT_xFNZ5EUxn8_T2XVeX5dX1TKUP91__VGrQPhxP7gN70G9eUGdlVdHavW2WY7LcXz1E6tHnxs4BQAA)

XML Feed: [https://debconf23.debconf.org/schedule/pentabarf.xml](/schedule/pentabarf.xml)

### Download Giggity

- Home page: [https://gaa.st/giggity](https://gaa.st/giggity)
- F-Droid: [https://f-droid.org/repository/browse/?fdid=net.gaast.giggity](https://f-droid.org/repository/browse/?fdid=net.gaast.giggity)
- Google Play Store: [https://play.google.com/store/apps/details?id=net.gaast.giggity
](https://play.google.com/store/apps/details?id=net.gaast.giggity)

### Download ConfClerk

	# apt install confclerk

### Download confy

	# apt install confy

which works well on debian based smartphones.

## iCalendar:

The schedule is available through an iCalendar (ical) feed.
Your calendar application should be able to consume this.

iCalendar Feed: [https://debconf23.debconf.org/schedule/schedule.ics](/schedule/schedule.ics)
