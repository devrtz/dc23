---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **JUNE** |                                                                   |
|----------|-------------------------------------------------------------------|
| 17       | Opening of the [Call For Proposals](/cfp/).                       |
| 17       | Opening attendee registration.                                    |

| **JULY** |                                                                   |
|----------|-------------------------------------------------------------------|
| 1        | Last date to apply for a bursary.                                 |
| 9        | Last day for submitting a talk that will be surely considered for the official schedule and have a response by July 23rd. |
| 23       | Acceptance notifications for talks submitted before July 9 will be sent around this day. |
| 24       | Last day for bursary team review travel requests and make 1st round of offers to bursary recipients. |
| 29       | Last day for 1st round of bursary recipients to accept.           |

| **AUGUST** |                                                                 |
|------------|-----------------------------------------------------------------|
| 1          | Last date to let us know if you need a visa to India (email: visa@debconf.org with a copy of your passport). |
| 5          | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |
| 13         | Last day to submit to proposal be considered for the main conference schedule, with video coverage guaranteed. |

| **SEPTEMBER**  |                                                             |
|----------------|-------------------------------------------------------------|
| *DebCamp*      |                                                             |
| 3 (Sunday)     | First day of DebCamp / Arrival day for DebCamp              |
| 4 (Monday)     | Second day of DebCamp                                       |
| 5 (Tuesday)    | Third day of DebCamp                                        |
| 6 (Wednesday)  | Fourth day of DebCamp                                       |
| 7 (Thursday)   | Fifth day of DebCamp                                        |
| 8 (Friday)     | Sixth day of DebCamp                                        |
| 9 (Saturday)   | Seventh day of DebCamp / Arrival day for DebConf            |
| *DebConf*      |                                                             |
| 10 (Sunday)    | First day of DebConf / Opening / Job fair                   |
| 11 (Monday)    | Second day of DebConf / Cheese and wine party (Four Points) |
| 12 (Tuesday)   | Third day of DebConf / Sadya (Four Points)                  |
| 13 (Wednesday) | Fourth day of DebConf / Day trip                            |
| 14 (Thursday)  | Fifth day of DebConf / Conference dinner (Bolgatty)         |
| 15 (Friday)    | Sixth day of DebConf                                        |
| 16 (Saturday)  | Seventh day of DebConf                                      |
| 17 (Sunday)    | Last day of DebConf / Closing ceremony / After party (Four Points) |
| 18 (Monday)    | Departure day                                               |
